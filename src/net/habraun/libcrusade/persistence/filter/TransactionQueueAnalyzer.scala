/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



import scala.actors._



/**
 * Evaluates all transactions from the transaction queue and stores those that can be stored. Repeats
 * until no further transactions can be stored.
 *
 * Returns the updated transaction queue.
 */

private[filter] class TransactionQueueAnalyzer(evaluate: (DataStore#Transaction) => Boolean, storage: Actor)
		extends Function1[TransactionReorderingFilter#TransactionList,
		TransactionReorderingFilter#TransactionList] {
	
	def apply(queue: TransactionReorderingFilter#TransactionList):
			TransactionReorderingFilter#TransactionList = {
		val partitionLists = queue.partition(evaluate)
		val storableTransactions = partitionLists._1
		val newQueue = partitionLists._2
		
		// Store all storable transactions.
		storableTransactions.foreach((transaction) => {
			storage ! Store(transaction)
		})
		
		// Check if any transactions could be stored.
		if (storableTransactions == Nil) {
			// No transactions could be stored. Return the updated queue.
			newQueue
		}
		else {
			// Transactions could stored. This could mean that more transactions from the queue can be saved
			// now. Evaluate the rest of the queue.
			apply(newQueue)
		}
	}
}

