/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



import scala.actors._
import scala.collection.immutable._



/**
 * Retrieve the object with the given key from a storage actor (usually a DataStore instance). Searches the
 * given cache for objects.
 */

private[filter] class StorageRetriever(storage: Actor) extends Function1[String, Option[Any]] {
	
	def apply(key: String): Option[Any] = {
		val valueFromStorage = storage !? Retrieve(key)
		if (valueFromStorage != null) {
			Some(valueFromStorage)
		}
		else {
			None
		}
	}
}

