/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



import scala.actors._
import scala.collection.immutable._



/**
 * This is a filter that can be used to reorder transactions before they reach the data store.
 * Reordering transactions may be necessary, if the application cannot guarantee that transactions are handed
 * to the data store in the logical order that was intended.
 *
 * This actor accepts the same messages as DataStore and can be used completely transparently. It will
 * directly relay any other messages than Store to the wrapped DataStore instance. It will hold back Store
 * messages that it receives out of order.
 *
 * As objects that are saved are game-specific, this filter can't decide the correct order by itself. A
 * comparator function will do this.
 * The comparator function takes two versions of the same object. If the first version directly precedes the
 * second one (thus, if the second version can be stored, if the first one is the one currently in the
 * database), the comparator should return true, otherwise it should return false. The first parameter is an
 * Option and may be None, if no previous version of the object exists.
 */

class TransactionReorderingFilter(storage: Actor, compare: (Option[Any], Any) => Boolean) extends Actor {
	
	type TransactionList = List[DataStore#Transaction]
	
	
	
	val retrieve = new StorageRetriever(storage)
	val evaluate = new TransactionEvaluator(compare, retrieve)
	val analyze = new TransactionQueueAnalyzer(evaluate, storage)



	start
	
	
	
	def act {
		loop(Nil)
	}
	
	
	
	private def loop(transactionQueue: TransactionList) {
		react {
			case Store(transaction) =>
				val newTransactionQueue = analyze(transaction::transactionQueue)
				loop(newTransactionQueue)
			
			case Retrieve(key) =>
				reply(storage !? Retrieve(key))
				loop(transactionQueue)
			
			case Delete(key) =>
				reply(storage !? Delete(key))
				loop(transactionQueue)
		}
	}
}

