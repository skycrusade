/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



/**
 * Evaluates if a transaction can be stored.
 */

private[filter] class TransactionEvaluator(compare: (Option[Any], Any) => Boolean,
		retrieve: (String) => Option[Any]) extends Function1[DataStore#Transaction, Boolean] {
	
	def apply(transaction: DataStore#Transaction): Boolean = {
		// Iterate through all objects of the transaction, check each of them using the comparator.
		transaction.forall((keyValuePair) => {
			val key = keyValuePair._1
			val value = keyValuePair._2
			
			// Compare the version in storage with the current version.
			compare(retrieve(key), value)			
		})
	}
}

