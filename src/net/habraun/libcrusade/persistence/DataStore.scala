/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence



import scala.actors._
import scala.collection.immutable._

import org.neodatis.odb._
import org.neodatis.odb.core.query.criteria._
import org.neodatis.odb.impl.core.query.criteria._



/**
 * Stores the objects given as Tuples. The first value of the tuple (String) being the key by which the value
 * is referred to, the other one (Any) being the value itself.
 */

case class Store(transaction: DataStore#Transaction)



/**
 * Tells DataStore to retrieve the object with the given key from the database. DataStore will send the
 * retrieved object as a reply. It will send null, if an object with that key doesn't exist in the database.
 */

case class Retrieve(key: String)



/**
 * Tells DataStore to delete the object with the given key. DataStore will reply with the deleted object, if
 * an object with that key existed and could be deleted. It will reply null if that was not the case.
 */

case class Delete(key: String)



/**
 * Saves and retrieves data to/from a store.
 */

class DataStore(dataSource: String) extends Actor {
	
	type Transaction = List[(String, Any)]
	
	
	
	val odb = ODBFactory.open("data/" + dataSource + ".odb")
	start
	
	
	
	def act {
		loop
	}
	
	
	
	private def loop {
		react {
			case Store(transaction) =>
				transaction.foreach((transactionObject) => {
					retrieveAndDo(transactionObject._1, () => {
						// No object was returned, the object has not been stored previously. Store the
						// object.
						odb.store(StoredObject(transactionObject._1, transactionObject._2))
					}, (storedObject: StoredObject) => {
						// One object was returned, the object has been stored previously. Delete the old
						// object and store the new one.
						odb.delete(storedObject)
						odb.store(StoredObject(transactionObject._1, transactionObject._2))
					})
				})
				
				odb.commit
				loop
			
			case Retrieve(key) =>
				retrieveAndDo(key, () => {
					// No object was returned. Return null.
					reply(null)
				}, (storedObject: StoredObject) => {
					// One object was returned. Return it.
					reply(storedObject.value)
				})
				
				loop
			
			case Delete(key) =>
				retrieveAndDo(key, () => {
					// No object was returned. Return null.
					reply(null)
				}, (storedObject: StoredObject) => {
					// One object was returned. Delete and return it.
					odb.delete(storedObject)
					odb.commit
					reply(storedObject)
				})
				
				loop
		}
	}
	
	
	
	private def retrieveAndDo(key: String, noObject: () => Unit, oneObject: (StoredObject) => Unit) {
		// Retrieve all stored objects with the given key.
		val query = new CriteriaQuery(classOf[StoredObject], Where.equal("key", key))
		val objects = odb.getObjects(query).toArray
		
		// Check how many objects where returned.
		if (objects.length == 0) {
			noObject()
		}
		else if (objects.length == 1) {
			val storedObject = objects(0).asInstanceOf[StoredObject]
			oneObject(storedObject)
		}
		else {
			// More than one object was returned. This should never happen as all keys are
			// supposed to be unique in the database.
			throw new AssertionError("Database corrupted. " + objects.length + " objects with key \""
					+ key + "\" in the database.")
		}
	}
}

