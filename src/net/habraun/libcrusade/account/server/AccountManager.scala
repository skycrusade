/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.account.server



import persistence._

import scala.actors._
import scala.collection.immutable._



/**
 * Creates an account. AccountManager will reply with a Boolean that indicates the success of the operation.
 * If AccountManager replies false, this usually means that an account of that name already existed.
 */

case class CreateAccount(name: String, password: String, attachment: Any)



/**
 * Deletes an account. AccountManager will reply with the deleted account, if it existed. It will reply null
 * if it did not.
 */

case class DeleteAccount(name: String)



/**
 * Logs an account in. AccountManager will reply with one of the following messages: LoginSuccessful,
 * AccountDoesNotExist, PasswordIncorrect or AccountAlreadyLoggedIn.
 */

case class LoginAccount(name: String, password: String)

case class LoginSuccessful(account: Account)
case class AccountDoesNotExist()
case class PasswordIncorrect()
case class AccountAlreadyLoggedIn()



/**
 * Logs an account out. Replies with a Boolean value that indicates the success of the operation.
 */

case class LogoutAccount(name: String)



/**
 * Manages accounts.
 */

class AccountManager(dataStore: Actor) extends Actor {
	
	start
	
	
	
	override def act() {
		loop(new HashMap[String, Account])
	}
	
	
	
	private def loop(loggedInAccounts: Map[String, Account]) {
		react {
			case CreateAccount(name, password, attachment) =>
				// Determine if the name is already taken by querying the data store for all accounts of
				// that name.
				val nameTaken = dataStore !? Retrieve("account." + name) != null
				
				// If the name is not taken create a new account and reply true. If it is, reply false.
				if (!nameTaken) {
					val newAccount = Account(name, password, attachment)
					dataStore ! Store(("account." + name, newAccount)::Nil)
					reply(true)
				}
				else {
					reply(false)
				}
				
				// No changes to the logged in accounts, so we'll just pass it on into the next
				// iteration.
				loop(loggedInAccounts)
			
			case DeleteAccount(name) =>
				// Tell the data store to delete the account with the given name. The store will return
				// the account that was deleted or null if there was no account of that name.
				val deletedAccount = (dataStore !? Delete("account." + name)).asInstanceOf[Account]
				
				// Check if an account was deleted.
				if (deletedAccount == null) {
					// No account was deleted, which means no account of that name existed. Return null.
					reply(null)
					loop(loggedInAccounts)
				}
				else {
					// An account has been deleted. Determine if the deleted account was logged in. If it
					// was, remove it from loggedInAccounts.
					val newLoggedInAccounts = {
						if (loggedInAccounts.contains(deletedAccount.name)) {
							loggedInAccounts - deletedAccount.name
						}
						else {
							loggedInAccounts
						}
					}
					
					// Return the deleted account.
					reply(deletedAccount)
					
					// Pass the new set of logged in accounts to the next loop iteration.
					loop(newLoggedInAccounts)
				}
			
			case LoginAccount(name, password) =>
				// Retrieve the account we'd like to log in.
				val account = (dataStore !? Retrieve("account." + name)).asInstanceOf[Account]
				
				// Check if the account could be retrieved.
				if (account == null) {
					// No account was returned, an account of that name doesn't exist.
					reply(AccountDoesNotExist)
					loop(loggedInAccounts)
				}
				else {
					// An account was returned. Check if the password was correct and if the account is
					// already logged in.
					if (account.password == password) {
						// The password is correct. Proceed with the check.
						if (!loggedInAccounts.contains(account.name)) {
							// Account is not logged in, so it is ok to log it in.
							reply(LoginSuccessful(account))
							loop(loggedInAccounts + (account.name -> account))
						}
						else {
							// The account is already logged in.
							reply(AccountAlreadyLoggedIn)
							loop(loggedInAccounts)
						}
					}
					else {
						// The password is incorrect.
						reply(PasswordIncorrect)
						loop(loggedInAccounts)
					}
				}
			
			case LogoutAccount(name) =>
				if (loggedInAccounts.contains(name)) {
					reply(true)
					loop(loggedInAccounts - name)
				}
				else {
					reply(false)
					loop(loggedInAccounts)
				}
				
		}
	}
}

