/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.network.server



import java.net._

import org.apache.mina.common._
import org.apache.mina.filter.codec._
import org.apache.mina.filter.codec.serialization._
import org.apache.mina.transport.socket.nio._



/**
 * Creates an acceptor for the given address and accepts all incoming connections. Creates an actor for each
 * connection using the provided actorFactory function. Routes network events to the responsible actor.
 */

class ClientManager(address: SocketAddress, initialHandler: ClientHandler, initialData: Any)
		extends IoHandlerAdapter {
	
	val initialGroup = new ClientGroup(initialHandler)
	
	val acceptor = new SocketAcceptor
	acceptor.getFilterChain.addFirst("codec", new ProtocolCodecFilter(new ObjectSerializationEncoder,
			new ObjectSerializationDecoder))
	acceptor.bind(address, this)
	
	
	
	/**
	 * Reacts to an incoming connection request.
	 * Creates a connection actor a the new session and starts it. Adds the new actor to the initial
	 * connection group.
	 */
	
	override def sessionOpened(session: IoSession) {
		val connection = new ClientConnection(session, initialData)
		connection ! AddToGroup(initialGroup)
		session.setAttachment(connection)
	}
	
	
	
	/**
	 * Reacts to a connection-closed event. Forwards the event to the connection actor.
	 */
	
	override def sessionClosed(session: IoSession) {
		session.getAttachment.asInstanceOf[ClientConnection] ! ConnectionClosed
	}
	
	
	
	/**
	 * Forwards incoming messages to the actor that is responsible for the session the message came from.
	 */
	
	override def messageReceived(session: IoSession, message: Any) {
		session.getAttachment.asInstanceOf[ClientConnection] ! MessageReceived(message)
	}
}

