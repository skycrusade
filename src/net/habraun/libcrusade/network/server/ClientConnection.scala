/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.network.server



import scala.actors._

import org.apache.mina.common._



/**
 * Sends a network message to the connection.
 */

case class Send(message: Any)



/**
 * Sends a network message if a condition is met. The condition closure takes the connection data as
 * parameter and must return a boolean.
 */

case class SendOnCondition(message: Any, condition: (Any) => Boolean)



/**
 * Adds the connection to the given group.
 */

case class AddToGroup(group: ClientGroup)



/**
 * Closes the connection.
 */

case class Close()



/**
 * Notifies the actor that a message has been received.
 */

private[server] case class MessageReceived(message: Any)



/**
 * Notifies the actor that the connection has been closed.
 */

private[server] case class ConnectionClosed()



/**
 * Handles network events for one connection.
 */

class ClientConnection(session: IoSession, initialData: Any) extends Actor {
	
	start
	
	
	
	override def act {
		loop(NoGroup, initialData)
	}
	
	
	
	private def loop(group: ClientGroup, data: Any) {
		react {
			case Send(message) =>
				session.write(message)
				loop(group, data)
			
			case SendOnCondition(message, condition) =>
				if (condition(data)) {
					session.write(message)
				}
				loop(group, data)
			
			case AddToGroup(newGroup) =>
				group ! RemoveConnection(this)
				newGroup ! AddConnection(this)
				val dataAfterRemove = group.getHandler.removedFromGroup(this, group, data)
				val dataAfterAdd = newGroup.getHandler.addedToGroup(this, newGroup,	dataAfterRemove)
				loop(newGroup, dataAfterAdd)
			
			case Close() =>
				session.close
				loop(group, data)
			
			case MessageReceived(message) =>
				val newData = group.getHandler.messageReceived(this, group, data, message)
				loop(group, newData)
			
			case ConnectionClosed() =>
				group ! RemoveConnection(this)
				group.getHandler.removedFromGroup(this, group, data)
				exit
		}
	}
}



/**
 * Represents no connection. Can be used everywhere a reference to a client connection is needed, but no
 * client is actually connected.
 */

object NoConnection extends ClientConnection(null, null) {
	override def act {}
}

