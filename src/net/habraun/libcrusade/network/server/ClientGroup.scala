/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.network.server



import scala.actors._
import scala.collection.immutable._



/**
 * Broadcasts a message.
 */

case class Broadcast(message: Any)



/**
 * Broadcasts a message. The message is only sent to a connection if the condition is met for that
 * connection.
 */

case class BroadcastOnCondition(message: Any, condition: (Any) => Boolean)



/**
 * Adds a connection to the group.
 */

private[server] case class AddConnection(connection: ClientConnection)



/**
 * Removes a connection from the group.
 */

private[server] case class RemoveConnection(connection: ClientConnection)



/**
 * Manages network connections.
 */

class ClientGroup(handler: ClientHandler) extends Actor {
	
	start
	
	
	
	/**
	 * Returns the handler for this connection group.
	 */
	
	private[server] def getHandler: ClientHandler = handler
	
	
	
	override def act {
		loop(new HashSet[ClientConnection])
	}
	
	
	
	private def loop(connections: Set[ClientConnection]) {
		react {
			case Broadcast(message) =>
				connections.foreach((connection) =>	connection ! Send(message))
				loop(connections)
				
			case BroadcastOnCondition(message, condition) =>
				connections.foreach((connection) =>	connection ! SendOnCondition(message, condition))
				loop(connections)
			
			case AddConnection(connection) =>
				loop(connections + connection)
				
			case RemoveConnection(connection) =>
				loop(connections - connection)
		}
	}
}



/**
 * NoGroup is a special ClientGroup instance that can be used if a connection is actually in no group. It
 * ignores all messages.
 */

object NoGroup extends ClientGroup(new ClientHandler {}) {

	override def act {}
}

