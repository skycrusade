/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.network.client



import java.net._
import scala.actors._

import org.apache.mina.common._
import org.apache.mina.filter.codec._
import org.apache.mina.filter.codec.serialization._
import org.apache.mina.transport.socket.nio._




/**
 * Sends a message to the server.
 */

case class Send(message: Any)



/**
 * Closes the connection.
 */

case class Close()



/**
 * Notifies ServerConnection that a message has been received.
 */

private[client] case class MessageReceived(message: Any)



/**
 * Notifies ServerConnection that the connection has been closed.
 */

private[client] case class ConnectionClosed()



/**
 * Encapsulates a connection to a server.
 */

class ServerConnection(address: SocketAddress, handler: ServerHandler) extends Actor {

	start
	
	
	
	def act {
		val connector = new SocketConnector()
		connector.getFilterChain.addFirst("codec", new ProtocolCodecFilter(new ObjectSerializationEncoder,
				new ObjectSerializationDecoder))
		
		val actor = this
		val handler = new IoHandlerAdapter {
			override def messageReceived(session: IoSession, message: Any) {
				actor ! MessageReceived(message)
			}
			
			override def sessionClosed(session: IoSession) {
				actor ! ConnectionClosed
			}
		}
		
		val connectFuture = connector.connect(address, handler)
		
		connectFuture.join
		loop(connectFuture.getSession)
	}
	
	
	
	private def loop(session: IoSession) {
		react {
			case Send(message) =>
				session.write(message)
				loop(session)
			
			case Close() =>
				session.close
				loop(session)
			
			case MessageReceived(message) =>
				handler.messageReceived(message)
				loop(session)
			
			case ConnectionClosed() =>
				handler.connectionClosed
				exit
		}
	}
}

