/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.common



import game._



/**
 * Wraps a text message that will be broadcasted to all clients.
 */

case class TextMessage(message: String)
case class BroadcastedTextMessage(sender: CommonProvinceData, message: String)



/**
 * Messages for account creation.
 */

case class CreateAccountMessage(accountName: String, password: String, provinceName: String,
		lordName: String)
case class AccountCreatedMessage(wasCreated: Boolean)



/**
 * Messages for login.
 */

case class LoginMessage(name: String, password: String)
case class LoggedInMessage(loginSuccessful: Boolean)



/**
 * Network messages for surrendering land to another province.
 */

case class SurrenderLand(size: Int, toProvince: String)



/**
 * Network message for changing the vote for the king election.
 */

case class ChangeVoteMessage(vote: String)



/**
 * Network message for assigning extension workers.
 */

case class AssignExtensionWorkersMessage(amount: Int)

