/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server



import game._
import net.habraun.skycrusade.common._

import net.habraun.libcrusade.account.server._
import net.habraun.libcrusade.network.server._



/**
 * Handles incoming messages for a connection.
 */

class InitialHandler(loggedInGroup: ClientGroup) extends ClientHandler {
	
	override def messageReceived(connection: ClientConnection, group: ClientGroup, data: Any, message: Any):
			Any = {
		val connectionData = data.asInstanceOf[ConnectionData]
		message match {
			case CreateAccountMessage(accountName, password, provinceName, lordName) =>
				if ((connectionData.accounts !? CreateAccount(accountName, password, provinceName))
						.asInstanceOf[Boolean]) {
					val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
					if ((kingdom !? AddProvince(provinceName, lordName)).asInstanceOf[Boolean]) {
						connection ! Send(AccountCreatedMessage(true))
					}
					else {
						connectionData.accounts ! DeleteAccount(accountName)
						connection ! Send(AccountCreatedMessage(false))
					}
				}
				else {
					connection ! Send(AccountCreatedMessage(false))
				}
				
				data
			
			case LoginMessage(name, password) =>
				val outcome = connectionData.accounts !? LoginAccount(name, password)
				outcome match {
					case LoginSuccessful(account) =>
						connection ! AddToGroup(loggedInGroup)
						connection ! Send(LoggedInMessage(true))
						ConnectionData(account, connectionData.accounts, connectionData.world)
					
					case _ =>
						connection ! Send(LoggedInMessage(false))
						data
				}
			
			case _ =>
				connection ! Close
				data
		}
	}
}

