/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import net.habraun.skycrusade.common.game._

import scala.actors._
import scala.collection.immutable._

import net.habraun.libcrusade.network.server._
import net.habraun.libcrusade.persistence._



/**
 * Returns the kingdom's data. Kingdom will reply with a KingdomData instance.
 */

case class GetKingdomData()



/**
 * Returns the kingdom's common data. Kingdom will reply with a CommonKingdomData instance.
 */

case class GetCommonKingdomData()



/**
 * Adds a province to the kingdom.
 */

case class AddProvince(provinceName: String, lordName: String)



/**
 * Removes a province from the kingdom.
 */

case class RemoveProvince(provinceName: String)



/**
 * Returns a province.
 */

case class GetProvince(provinceName: String)



/**
 * Moves land from one province to another. Replies with a boolean that indicates the success of the
 * operation.
 */

case class MoveLand(from: String, to: String, amount: Int)



/**
 * Changes a vote of a province.
 * Returns true if the message is valid and could be processed, false otherwise.
 */

case class ChangeVoteAndProcessElection(votingProvince: String, vote: String)



/**
 * Updates the kingdom and all its provinces. Replies with a KingdomData object.
 */

private[game] case class UpdateKingdom()



/**
 * An actor that wraps a Kingdom instance. Performs all tasks that are not directly related to the game
 * logic.
 */

class KingdomActor(data: KingdomData, dataStore: Actor) extends Actor {
	
	start
	
	
	
	/**
	 * Creates a new kingdom with the given name. This method is meant for creating completely new kingdoms
	 * that don't come from storage.
	 */
	
	def this(name: String, dataStore: Actor) {
		this(KingdomData(0, name, Nil, "", new EventList(5)), dataStore)
	}
	
	
	
	def act {
		if (data.index == 0) {
			dataStore ! Store(("kingdom." + data.name, data)::Nil)
		}

		val provinceData = loadProvinceData(data.provinces.toList, new HashMap[String, ProvinceData])
		val provinces = provinceData.transform((provinceName: String, provinceData: ProvinceData) =>
			new ProvinceActor(provinceData))
		loop(Kingdom(data.index, data.name, provinces, data.king, data.events))
	}
	
	
	
	private def loop(kingdom: Kingdom) {
		react {
			case GetKingdomData() =>
				reply(kingdom.generateData)
				loop(kingdom)
			
			case GetCommonKingdomData() =>
				reply(kingdom.generateCommonData)
				loop(kingdom)
			
			case AddProvince(provinceName, lordName) =>
				// Attempt to add the province. Check if adding the province succeeded.
				val newKingdom = kingdom.addProvince(provinceName, lordName)
				if (newKingdom != null) {
					// The province has been added. Store the added province and the changed kingdom data.
					val kingdomData = newKingdom.generateData
					val newProvinceData = (newKingdom.provinces(provinceName) !? GetProvinceData)
							.asInstanceOf[ProvinceData]
					val provinceDataTuple = ("province." + newProvinceData.name, newProvinceData)
					val kingdomDataTuple = ("kingdom." + kingdom.name, kingdomData)
					dataStore ! Store(provinceDataTuple::kingdomDataTuple::Nil)
					
					// Notify all connections that are attached to the kingdoms provinces about the change.
					notifyConnections(newKingdom.generateCommonData, kingdom.provinces)
					
					reply(true)
					loop(newKingdom)
				}
				else {
					// The province could not be added.
					reply(false)
					loop(kingdom)
				}
			
			case RemoveProvince(provinceName) =>
				// Remove the province from the kingdom.
				val newKingdom = kingdom.removeProvince(provinceName)
				
				// Delete the province from storage and save the modified kingdom data.
				val kingdomData = newKingdom.generateData
				val provinceData = (kingdom.provinces(provinceName) !? GetProvinceData)
						.asInstanceOf[ProvinceData]
				val kingdomDataTuple = ("kingdom." + kingdom.name, kingdomData)
				dataStore ! Store(kingdomDataTuple::Nil)
				dataStore ! Delete("province." + provinceName)
				
				// Notify the attached connections.
				notifyConnections(newKingdom.generateCommonData, newKingdom.provinces)
				
				loop(newKingdom)
			
			case GetProvince(provinceName) =>
				reply(kingdom.provinces(provinceName))
				loop(kingdom)
			
			case MoveLand(from, to, amount) =>
				// Attempt to move the land. Check if it succeeded.
				val resultOption = kingdom.moveLand(from, to, amount)
				if (resultOption.isDefined) {
					// Moving the land succeeded. Extract the updated kingdom instance and the updated
					// province data from the result tuple.
					val result = resultOption.getOrElse { throw new AssertionError }
					val newKingdom = result._1
					val newFrom = result._2
					val newTo = result._3
					
					// Save the changes to the data store.
					val fromDataTuple = ("province." + from, newFrom.generateData)
					val toDataTuple = ("province." + to, newTo.generateData)
					val kingdomTuple = ("kingdom." + newKingdom.name, newKingdom.generateData)
					dataStore ! Store(fromDataTuple::toDataTuple::kingdomTuple::Nil)
					
					// Notify the attached connections.
					notifyConnections(newKingdom.generateCommonData, kingdom.provinces)
					
					reply(true)
					loop(newKingdom)
				}
				else {
					reply(false)
					loop(kingdom)
				}
			
			case ChangeVoteAndProcessElection(votingProvince, vote) =>
				// Attempt to change the vote. Check if it succeeded.
				val result = kingdom.changeVoteAndProcessElection(votingProvince, vote)
				if (result != null) {
					// The operation succeeded. Extract the updated kingdom and the updated province data
					// from the result.
					val newKingdom = result._1
					val provinceData = result._2
					
					// Save the updated data.
					val kingdomTuple = ("kingdom." + kingdom.name, newKingdom.generateData)
					val provinceTuple = ("province." + provinceData.name, provinceData)
					dataStore ! Store(kingdomTuple::provinceTuple::Nil)
					
					// Notify the connected clients.
					notifyConnections(newKingdom.generateCommonData, kingdom.provinces)
					
					reply(true)
					loop(newKingdom)
				}
				else {
					// Operation was not successful.
					reply(false)
					loop(kingdom)
				}
			
			case UpdateKingdom() =>
				val result = kingdom.update
				val newKingdom = result._1
				val updatedProvinceData = result._2
				
				// Save the updated province data.
				val storableList = provinceDataMapToStorableList(updatedProvinceData, Nil)
				dataStore ! Store(storableList)
				
				loop(newKingdom)
		}
	}
	
	
	
	private def loadProvinceData(provinceNames: List[String], provinceData: Map[String, ProvinceData]):
			Map[String, ProvinceData] = {
		if (provinceNames.size == 0) {
			provinceData
		}
		else {
			val loadedProvinceData = (dataStore !? Retrieve("province." + provinceNames(0)))
					.asInstanceOf[ProvinceData]
			val loadedProvincePair = (provinceNames(0) -> loadedProvinceData)
			loadProvinceData(provinceNames.drop(1), provinceData + loadedProvincePair)
		}
	}
	
	
	
	/**
	 * Sends the given data object to all connections that are attached to the given provinces.
	 */
	
	private def notifyConnections(data: CommonKingdomData, provinces: Map[String, ProvinceActor]) {
		provinces.foreach((nameProvincePair) => {
			val province = nameProvincePair._2
			province.getPlayer ! Notify(data)
		})
	}
	
	
	
	/**
	 * Transforms a map with province names as keys and province data as values into a List that can be
	 * passed to DataStore.
	 */
	
	private def provinceDataMapToStorableList(dataMap: Map[String, ProvinceData],
			result: List[(String, Any)]): List[(String, Any)] = {
		if (dataMap.size == 0) {
			result
		}
		else {
			val provinceName = dataMap.keys.next
			val storableTuple = ("province." + provinceName, dataMap(provinceName))
			provinceDataMapToStorableList(dataMap - provinceName, storableTuple::result)
		}
	}
}

