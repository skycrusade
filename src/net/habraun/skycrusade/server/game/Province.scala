/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import net.habraun.skycrusade.common.game._



/**
 * Contains the game logic for provinces.
 */

case class Province(index: Long, name: String, lord: String, size: Int, population: Int,
		extensionWorkers: Int, vote: String) extends ProvinceConstants {
	
	/**
	 * Constructs a new province with the given name and lord. This constructor is meant for creating a
	 * completely new province, not one that comes from storage.
	 */
	
	def this(name: String, lord: String) {
		this(0, name, lord, 100, 5000, 0, "")
	}
	
	
	
	/**
	 * Generates a ProvinceData object.
	 */
	
	def generateData: ProvinceData = {
		ProvinceData(index, name, lord, size, population, extensionWorkers, vote)
	}
	
	
	
	/**
	 * Generates a CommonProvinceData object.
	 */
	
	def generateCommonData: CommonProvinceData = {
		CommonProvinceData(name, lord, size, vote)
	}
	
	
	
	/**
	 * Generates an OwnProvinceData object.
	 */
	
	def generateOwnData: OwnProvinceData = {
		OwnProvinceData(name, lord, size, population, extensionWorkers)
	}
	
	
	
	/**
	 * Returns a new Province object with a changed amount of land.
	 */
	
	def changeLand(amount: Int): Province = {
		// Determine what the new size would be if the given amount were added. If amount is negative
		// this might be below the minimum province size or even below zero.
		val nominalNewSize = size + amount
		
		// Determine the actual new size, which should be a valid size (at least the minimum size for
		// a province.
		val actualNewSize = Math.max(nominalNewSize, 20)
		
		// Return the changed data object.
		Province(index + 1, name, lord, actualNewSize, population, extensionWorkers, vote)
	}
	
	
	
	/**
	 * Changes the vote of the province.
	 */
	
	def changeVote(newVote: String): Province = {
		Province(index + 1, name, lord, size, population, extensionWorkers, newVote)
	}



	/**
	 * Assigns workers to extending the province.
	 * Returns Some(Province) if the workers could be assigned or None if not enough workers were available.
	 */

	def assignExtensionWorkers(amount: Int): Option[Province] = {
		// Check if there is enough population to assign these workers.
		if (population >= amount) {
			Some(Province(index + 1, name, lord, size, population, amount, vote))
		}
		else {
			None
		}
	}
	
	
	
	/**
	 * Updates the province.
	 * Currently, this only grows the population.
	 */
	
	def update: Province = {
		// Compute the population growth. This growth is governed by the following rules:
		// * If the population is below a certain threshold, a constant number of will be added.
		// * Otherwise, the part of the population that reproduces will be determined.
		// * The number of children the reproducing part produces is computed and added to the population.
		val newPopulation = if (population > POPULATION_MIN_GROWTH_THRESHOLD) {
			val procreatingPopulation = population - extensionWorkers
			val children = (procreatingPopulation * POPULATION_REGULAR_GROWTH).asInstanceOf[Int]
			Math.min(size * POPULATION_MAX_PER_SQKM, population + children)
		}
		else {
			population + POPULATION_MIN_GROWTH
		}

		// If workers are assigned to extend the province size, it will grow. Compute the new size.
		val newSize = size + (EXTENSION_PER_WORKER * extensionWorkers).asInstanceOf[Int]
		
		// Return the changed data object.
		Province(index + 1, name, lord, newSize, newPopulation, extensionWorkers, vote)
	}
}



trait ProvinceConstants {
	val POPULATION_MIN_GROWTH_THRESHOLD = 100
	val POPULATION_MIN_GROWTH = 10
	val POPULATION_REGULAR_GROWTH = 0.01
	val POPULATION_MAX_PER_SQKM = 200

	val EXTENSION_PER_WORKER = 0.5
}

