/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import net.habraun.skycrusade.common.game._

import scala.actors._



/**
 * Returns the province's data. Replies with a ProvinceData instance.
 */

case class GetProvinceData()



/**
 * Returns the province's common data. Replys with a CommonProvinceData instance.
 */

case class GetCommonProvinceData()



/**
 * Returns the data that is visible to the controlling player. Replies with an OwnProvinceData instance.
 */

case class GetOwnProvinceData()



/**
 * Assigns extension workers.
 */

case class AssignExtensionWorkers(amount: Int)



/**
 * Changes the amount of land the province has. Adds the given amount to the provinces current amount.
 * Returns a tuple with the amount of land that was added to the province and the updated province data.
 * 1. If land is taken away (amount is negative), the amount of land that is actually taken away from the
 *    province is returned.
 *    Example 1: The size of the province is 100, the amount taken away is 20 (amount = -20). Province
 *               returns 20 and the new size is 80.
 *    Example 2: The size of the province is 25, the amount taken away is 10 (amount = -10). Province returns
 *               5 and the new size is 20, because the minimum size of a province is 20.
 * 2. If land is given to the province (amount is positive), the amount of land that is actually added is
 *    returned as a negative number.
 *    Example: The size of the province is 100, the amount of land given to the province is 20 (amount = 20).
 *             Province returns -20, the new size is 120.
 *
 * Please note that there currently is no maximum size for provinces, but if there ever was one to be
 * implemented, the negative amount returned by Province could be different then -amount. Currently adding
 * land to a province always results in -amount to be returned.
 *
 * The amount of land is returned as the first part of a tuple, as described above. Additionally, the updated
 * Province instance is returned as the second part of the tuple.
 */

private[game] case class ChangeLand(amount: Int)



/**
 * Changes the vote of the province. Returns the updated province data.
 */

private[game] case class ChangeVote(newVote: String)



/**
 * Updates the province. Returns the updated province data.
 */

private[game] case class UpdateProvince()



/**
 * ProvinceActor is an actor that wraps a Province instance and performs all tasks that are not directly
 * related to game logic.
 */

class ProvinceActor(data: ProvinceData) extends Actor {
	
	private val player = new Player
	
	start
	
	
	
	/**
	 * Constructs a ProvinceActor instance with a new province.
	 */
	
	def this(name: String, lord: String) {
		this(new Province(name, lord).generateData)
	}
	
	
	
	/**
	 * Returns this province's player.
	 */
	
	def getPlayer: Player = player
	
	
	
	def act {
		loop(Province(data.index, data.name, data.lord, data.size, data.population, 0, data.vote))
	}
	
	
	
	private def loop(province: Province) {
		react {
			case GetProvinceData() =>
				reply(province.generateData)
				loop(province)
			
			case GetCommonProvinceData() =>
				reply(province.generateCommonData)
				loop(province)
			
			case GetOwnProvinceData() =>
				reply(province.generateOwnData)
				loop(province)

			case AssignExtensionWorkers(amount) =>
				// Assign the extension workers.
				val newProvinceOption = province.assignExtensionWorkers(amount)

				// Check if the operation succeeded and returned a valid Province.
				if (newProvinceOption.isDefined) {
					// It did. Retrieve the province from the option and notify the player.
					val newProvince = newProvinceOption.getOrElse { throw new AssertionError }
					player ! Notify(newProvince.generateOwnData)

					reply(true)
					loop(newProvince)
				}
				else {
					// Operation was not succesful.
					reply(false)
					loop(province)
				}
			
			case ChangeLand(amount) =>
				// Change the amount of land.
				val newProvince = province.changeLand(amount)
						
				// Send the reply to the kingdom. The reply consists of the actual amount of moved land, as
				// well as the update province data.
				// Removed amounts of land are always positive (removed from the province, _added_ to the
				// caller's virtual pool of land), added amounts always negative (added to the province,
				// _removed_ from the caller's virtual pool of land).
				reply((province.size - newProvince.size, newProvince))
				
				// Notify the player.
				player ! Notify(newProvince.generateOwnData)
				
				loop(newProvince)
			
			case ChangeVote(newVote) =>
				// Changes the vote and returns the new province data. This message is sent by the kingdom,
				// which then processes the new voting situation and saves the changes to the database.
				// Therefore, no database stuff to be done here.
				val newProvince = province.changeVote(newVote)
				reply(newProvince.generateData)
				loop(newProvince)
			
			case UpdateProvince() =>
				// Update the province.
				val newProvince = province.update
				
				// Return the updated data to the kingdom.
				reply(newProvince.generateData)
				
				// Notify the player.
				player ! Notify(newProvince.generateOwnData)
				
				loop(newProvince)
		}
	}
}

