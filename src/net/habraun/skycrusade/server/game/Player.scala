/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import scala.actors._

import net.habraun.libcrusade.network.server._



/**
 * Attaches a connection to this player.
 */

case class AttachConnection(connection: ClientConnection)



/**
 * Detaches the currently attached connection from the player.
 */

case class DetachConnection()



/**
 * If a connection is attached, the given message will be sent.
 */

case class Notify(message: Any)



/**
 * Encapsulates a connection to a player.
 */

class Player extends Actor {
	
	start
	
	
	
	def act {
		loop(NoConnection)
	}
	
	
	
	private def loop(connection: ClientConnection) {
		react {
			case AttachConnection(newConnection) =>
				if (connection != NoConnection) {
					throw new AssertionError("This province is already associated with a connection.")
				}
				loop(newConnection)
			
			case DetachConnection() =>
				if (connection == NoConnection) {
					throw new AssertionError("No connection is currently associated with this province.")
				}
				loop(NoConnection)
			
			case Notify(message) =>
				connection ! Send(message)
				loop(connection)
		}
	}
}

