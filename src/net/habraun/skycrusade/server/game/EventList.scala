/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import java.text._
import java.util.Date



/**
 * A kingdom's list of events.
 */

case class EventList(maxSize: Int, events: List[String]) {
	
	/**
	 * Constructor for constructing a new event list.
	 */
	
	def this(maxSize: Int) {
		this(maxSize, Nil)
	}
	
	
	
	/**
	 * Adds the given event to the event list. If the list is already at its maximum size, the oldest event
	 * is removed.
	 */
	
	def addEvent(event: String): EventList = {
		val eventsAfterDrop = {
			// Check if the event list is at its maximum size or exceeds it.
			if (events.size >= maxSize) {
				// There are too many events. Drop enough events, so there is room for one more.
				events.take(maxSize - 1)
			}
			else {
				// Not too many events. Nothing needs to be changed.
				events
			}
		}
		
		val timestamp = (new SimpleDateFormat("yyyy/MM/dd, HH:mm")).format(new Date)
		
		// Append the latest event and return it.
		EventList(maxSize, (timestamp + ": " + event)::eventsAfterDrop)
	}
}
