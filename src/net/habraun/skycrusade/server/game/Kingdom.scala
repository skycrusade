/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import net.habraun.skycrusade.common.game._

import scala.collection.immutable._



/**
 * Contains the game logic for kingdoms.
 */

case class Kingdom(index: Long, name: String, provinces: Map[String, ProvinceActor], king: String,
		events: EventList) {
	
	/**
	 * Returns the kingdom's data.
	 */
	
	def generateData: KingdomData = {
		KingdomData(index, name, provinces.keySet.toSeq, king, events)
	}
	
	
	
	/**
	 * Returns the kingdom's common data.
	 */
	
	def generateCommonData: CommonKingdomData = {
		val commonProvinceData = provinces.transform((key: String, value: ProvinceActor) => {
			(value !? GetCommonProvinceData).asInstanceOf[CommonProvinceData]
		})
		CommonKingdomData(name, commonProvinceData, king, events.events.toSeq)
	}
	
	
	
	/**
	 * Adds a province to the kingdom. Returns an updated Kingdom object if adding the province succeeds,
	 * returns null if it doesn't.
	 */
	
	def addProvince(provinceName: String, lordName: String): Kingdom = {
		// Check if the province name is already taken.
		if (!provinces.contains(provinceName) && provinceName != "") {
			// The province name is available. Create a new province and add it.
			val newProvince = new ProvinceActor(provinceName, lordName)
			val newProvinces = provinces + (provinceName -> newProvince)
			
			// Add an event to the event list.
			val newEvents = events.addEvent("Lord " + lordName + " of "	+ provinceName
					+ " has joined this kingdom.")
			
			val newKing = processElection(newProvinces)
			
			Kingdom(index + 1, name, newProvinces, newKing, newEvents)
		}
		else {
			null
		}
	}
	
	
	
	/**
	 * Removes the province. Returns an updated Kingdom instance.
	 */
	
	def removeProvince(provinceName: String): Kingdom =  {
		// Retrieve the name of the province's lord before removing it.
		val lordName = (provinces(provinceName) !? GetProvinceData).asInstanceOf[ProvinceData].lord
		
		// Remove the province.
		val newProvinces = provinces - provinceName
		val newEvents = events.addEvent("The treacherous Lord " + lordName + " of "	+ provinceName
				+ " has has left our kingdom!")
		
		val newKing = processElection(newProvinces)
		
		Kingdom(index + 1, name, newProvinces, newKing, newEvents)
	}
	
	
	
	/**
	 * Moves land from one province to another. Returns a tuple containing the updated Kingdom instance and
	 * the updated provinces, if moving the land succeeds. Returns None if it doesn't.
	 */
	
	def moveLand(from: String, to: String, amount: Int): Option[Tuple3[Kingdom, Province, Province]] = {
		// Check if the parameters are valid.
		if (from != to && amount > 0) {
			// Parameters are valid. Attempt to remove the amount of land from the "from" province. Add as
			// much land to the "to" province as could be removed from the "from" province.
			val fromResult = (provinces(from) !? ChangeLand(-amount)).asInstanceOf[Tuple2[Int, Province]]
			val amountOfRemovedLand = fromResult._1
			val toResult = (provinces(to) !? ChangeLand(amountOfRemovedLand))
					.asInstanceOf[Tuple2[Int, Province]]
			
			// Add an event to the event list.
			val newFrom = fromResult._2
			val newTo = toResult._2
			val newEvents = events.addEvent("Lord " + newFrom.lord + " of " + newFrom.name
					+ " surrendered " + amount + " square kilometers of land to lord " + newTo.lord
					+ " of " + newTo.name + ".")
			
			Some((Kingdom(index + 1, name, provinces, king, newEvents), newFrom, newTo))
		}
		else {
			// Parameters are invalid.
			None
		}
	}
	
	
	
	/**
	 * Changes a province's vote and processes the following election. Returns a tuple containing the
	 * updated Kingdom instance and the updated province data, if the operation succeeds. Returns null if it
	 * doesn't.
	 */
	
	def changeVoteAndProcessElection(votingProvince: String, vote: String): Tuple2[Kingdom, ProvinceData] = {
		// First check the validity of the parameters:
		// 1. The voting province must be in the kingdom.
		// 2. The province it votes for must be in the kingdom or the vote must be emtpy.
		if (provinces.contains(votingProvince) && (provinces.contains(vote)	|| vote == "")) {
			// Paramters are valid. Change the vote.
			val provinceData = (provinces(votingProvince) !? ChangeVote(vote)).asInstanceOf[ProvinceData]
			
			val newKing = processElection(provinces)
			
			(Kingdom(index + 1, name, provinces, newKing, events), provinceData)
		}
		else {
			null
		}
	}
	
	
	
	/**
	 * Processes the election. Returns the name of the new king or "", if no king has been elected.
	 */
	
	private def processElection(votingProvinces: Map[String, ProvinceActor]): String = {
		// Process the election.
		val provincesData = votingProvinces.transform((provinceName, province) => {
			(province !? GetProvinceData).asInstanceOf[ProvinceData]
		})
		val votes = countVotes(provincesData, new HashMap[String, Int])
		val kingMap = votes.filter((votesTuple) => {
			votesTuple._2 > 0.5 * votingProvinces.size
		})
		if (kingMap.size > 1) {
			throw new AssertionError("More than one province has more than 50% of all votes.")
		}
		val newKing = if (kingMap.size == 0) {
			""
		}
		else {
			kingMap.keySet.elements.next
		}
		
		newKing
	}
	
	
	
	/**
	 * Updates the kingdom and all its provinces. Returns a tuple containing the updated Kingdom instance and
	 * a map containing the updated province data.
	 */
	
	def update: Tuple2[Kingdom, Map[String, ProvinceData]] = {
		// Update all provinces of this kingdom. Request a future for the updated province data that
		// province will reply with and save all the futures in a map.
		val futures = provinces.transform((provinceName, province) => {
			province !! UpdateProvince
		})
		
		// Iterate over all futures and wait for each future until the reply has arrived. Save the
		// replies (the updated province data) in yet another map.
		val updatedData = futures.transform((provinceName, future) => {
			future().asInstanceOf[ProvinceData]
		})
		
		(this, updatedData)
	}
	
	
	
	private def countVotes(provinces: Map[String, ProvinceData], votes: Map[String, Int]):
			Map[String, Int] = {
		if (provinces.isEmpty) {
			votes
		}
		else {
			val province = provinces.values.next
			val updatedVotes = if (votes.contains(province.vote)) {
				votes.update(province.vote, votes(province.vote) + 1)
			}
			else {
				votes + (province.vote -> 1)
			}
			countVotes(provinces - province.name, updatedVotes)
		}
	}
}

