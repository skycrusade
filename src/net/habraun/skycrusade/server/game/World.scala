/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import net.habraun.skycrusade.common.game._

import scala.actors._
import scala.collection.immutable._

import net.habraun.libcrusade.network.server._
import net.habraun.libcrusade.persistence._



/**
 * Sends the global kingdom to the reply recipient.
 */

case class GetKingdom()



/**
 * Updates the world itself and causes it to update all kingdoms. This message triggers game logic that is
 * meant to run at regular intervals, not as reaction to input events. An example for this kind of logic is
 * the population growth of a province.
 */

case class UpdateWorld()



/**
 * Top-level container for all game data.
 */

class World(dataStore: Actor) extends Actor {
	
	start
	
	
	
	override def act {
		// Set the default name for the global kingdom and try to retrieve it from storage.
		val kingdomName = "Global Kingdom"
		val kingdomData = (dataStore !? Retrieve("kingdom." + kingdomName)).asInstanceOf[KingdomData]
		
		// Check if the global kingdom could be retrieved from storage.
		if (kingdomData != null) {
			// The global kingdom could be retrieved. Create a new Kingdom instance from the retrieved data.
			loop(new KingdomActor(kingdomData, dataStore))
		}
		else {
			// The global kingdom could not be retrieved. Create a new kingdom with the default name.
			loop(new KingdomActor(kingdomName, dataStore))
		}
	}
	
	
	
	private def loop(kingdom: KingdomActor) {
		react {
			case GetKingdom() =>
				reply(kingdom)
				loop(kingdom)
			
			case UpdateWorld() =>
				kingdom ! UpdateKingdom
				loop(kingdom)
		}
	}
}

