/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server



import game._
import net.habraun.skycrusade.common._
import net.habraun.skycrusade.common.game._

import net.habraun.libcrusade.account.server._
import net.habraun.libcrusade.network.server._



/**
 * A ConnectionHandler instance for logged in clients.
 */

class LoggedInHandler extends ClientHandler {
	
	/**
	 * Attaches the connection to the account's province.
	 */
	
	override def addedToGroup(connection: ClientConnection, group: ClientGroup, data: Any): Any = {
		val connectionData = data.asInstanceOf[ConnectionData]
		val provinceName = connectionData.account.attachment.asInstanceOf[String]
		
		// Attach the connection to the account's province.
		val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
		val province = (kingdom !? GetProvince(provinceName)).asInstanceOf[ProvinceActor]
		province.getPlayer ! AttachConnection(connection)
		
		// Send initial province and kingdom information.
		val kingdomData = (kingdom !? GetCommonKingdomData).asInstanceOf[CommonKingdomData]
		val provinceData = (province !? GetOwnProvinceData).asInstanceOf[OwnProvinceData]
		connection ! Send(provinceData)
		connection ! Send(kingdomData)
		
		data
	}
	
	
	
	/**
	 * Logs the account out and detaches the connection from the account's province.
	 */
	
	override def removedFromGroup(connection: ClientConnection, group: ClientGroup, data: Any): Any = {
		val connectionData = data.asInstanceOf[ConnectionData]
		
		// Log the account out.
		connectionData.accounts ! LogoutAccount(connectionData.account.name)
		
		// Detach the connection from the account's province.
		val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
		val provinceName = connectionData.account.attachment.asInstanceOf[String]
		val province = (kingdom !? GetProvince(provinceName)).asInstanceOf[ProvinceActor]
		province.getPlayer ! DetachConnection
		
		data
	}
	
	
	
	/**
	 * Handles a received message.
	 */
	
	override def messageReceived(connection: ClientConnection, group: ClientGroup, data: Any,
			message: Any): Any = {
		val connectionData = data.asInstanceOf[ConnectionData]
		message match {
			case TextMessage(textMessage) =>
				val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
				val sender = (kingdom !? GetProvince(connectionData.account.attachment.asInstanceOf[String]))
						.asInstanceOf[ProvinceActor]
				val commonSenderData = (sender !? GetCommonProvinceData).asInstanceOf[CommonProvinceData]
				group ! Broadcast(BroadcastedTextMessage(commonSenderData, textMessage))
				data
			
			case SurrenderLand(size, targetProvince) =>
				val ownProvince = connectionData.account.attachment.asInstanceOf[String]
				val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
				kingdom ! MoveLand(ownProvince, targetProvince, size)
				data
			
			case ChangeVoteMessage(vote) =>
				val ownProvince = connectionData.account.attachment.asInstanceOf[String]
				val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
				val success = (kingdom !? ChangeVoteAndProcessElection(ownProvince, vote))
						.asInstanceOf[Boolean]
				if (!success) {
					// Kingdom returned false, indicating that the message was invalid.
					connection ! Close
				}
				data
			
			case AssignExtensionWorkersMessage(amount) =>
				val ownProvinceName = connectionData.account.attachment.asInstanceOf[String]
				val kingdom = (connectionData.world !? GetKingdom).asInstanceOf[KingdomActor]
				val ownProvince = (kingdom !? GetProvince(ownProvinceName)).asInstanceOf[ProvinceActor]
				val success = (ownProvince !? AssignExtensionWorkers(amount)).asInstanceOf[Boolean]
				if (!success) {
					// The province returned false, indicating that the amount was invalid.
					connection ! Close
				}
				data
				
			case _ =>
				connection ! Close
				data
		}
	}
}

