/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server



import game._

import java.net._

import net.habraun.libcrusade.account.server._
import net.habraun.libcrusade.network.server._
import net.habraun.libcrusade.persistence._
import net.habraun.libcrusade.persistence.filter._



/**
 * The main object and entry point for the Sky Crusade server application.
 */

object SkyCrusadeServer extends Application {
	
	// Create an instance of LoggedInHandler, the handler that will handle connections that have logged into
	// an account. Create a ClientGroup instance for these clients and pass it the handler.
	val loggedInHandler = new LoggedInHandler
	val loggedInGroup = new ClientGroup(loggedInHandler)
	
	// Instantiate InitialHandler, which will handle connections that have not yet logged into an account.
	// This handler will direct clients to the loggedInGroup.
	val initialHandler = new InitialHandler(loggedInGroup)

	// Create a comparator that will, together with TransactionReorderingFilter, make sure that data is saved
	// in the right order, so new data won't get overwritten by older data that got held up.
	val comparator = (previousValue: Option[Any], currentValue: Any) => {
		val current = currentValue.asInstanceOf[GameData]

		// Check if there is a previous value.
		if (previousValue.isDefined) {
			// There is a previous value. Retrieve it from the option and compare it to the current value.
			val previous = previousValue.getOrElse({ throw new AssertionError }).asInstanceOf[GameData]
			previous.index + 1 == current.index
		}
		else {
			// No previous value. The current value can be stored if its index is 0.
			current.index == 0
		}
	}
	
	// Instantiate utility objects that are provided by the framework.
	val dataStore = new DataStore("skycrusade")
	val filter = new TransactionReorderingFilter(dataStore, comparator)
	val accountManager = new AccountManager(dataStore)
	
	// Instantiate the game world, which contains all game logic and data.
	val world = new World(filter)
	
	// Instantiate the initial ConnectionData instance. This contains references to the account manager and
	// the game world and will be given to all new connections.
	val initialData = ConnectionData(null, accountManager, world)
	
	// Instantiate ClientManager. ClientManager will accept incoming connections and adds them to an initial
	// group whose clients are handled by the initial handler.
	new ClientManager(new InetSocketAddress(34482), initialHandler, initialData)
	
	// Create and start a thread that will update the game world in regular intervals.
	new Thread(new Runnable {
		def run {
			while (true) {
				world ! UpdateWorld
				Thread.sleep(1000 * 60 * 10)
			}
		}
	}).start
}

