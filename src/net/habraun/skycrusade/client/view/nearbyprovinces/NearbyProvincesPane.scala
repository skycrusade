/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view.nearbyprovinces



import net.habraun.skycrusade.common.game._

import java.awt._
import java.awt.event._
import javax.swing._

import info.clearthought.layout._



/**
 * Show the provinces of a player's kingdom and other kingdom's on the same island.
 */

class NearbyProvincesPane extends JPanel {
	
	val yourKingdomLabel = new JLabel("Your Kingdom")
	val yourKingdomProvinces = new KingdomProvincesPane
	val yourKingdomProvincesScrollPane = new JScrollPane(yourKingdomProvinces)
	
	val otherKingdomsLabel = new JLabel("Other Kingdoms")
	val otherKingdoms = new JPanel
	val otherKingdomsScrollPane = new JScrollPane(otherKingdoms)
	
	setLayout(new TableLayout(Array(0.01, 0.98, 0.01), Array(0.05, 0.45, 0.05, 0.05, 0.4)))
	
	add(yourKingdomLabel, "0, 0, 2, 0, CENTER, CENTER")
	add(yourKingdomProvincesScrollPane, "1, 1")
	add(otherKingdomsLabel, "0, 3, 2, 3, CENTER, CENTER")
	add(otherKingdomsScrollPane, "1, 4")
	
	
	
	/**
	 * Adds a listener for land surrendering actions.
	 */
	
	def addSurrenderListener(surrenderListener: (Int, CommonProvinceData) => Unit) {
		yourKingdomProvinces.addSurrenderListener(surrenderListener)
	}
	
	
	
	/**
	 * Displays the given kingdom.
	 */
	
	def display(kingdom: CommonKingdomData, ownProvince: String) {
		yourKingdomProvinces.display(kingdom, ownProvince)
	}
}

