/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view.nearbyprovinces



import net.habraun.skycrusade.common.game._

import javax.swing._

import info.clearthought.layout._



/**
 * Displays the provinces of a kingdom.
 */

private[nearbyprovinces] class KingdomProvincesPane extends JPanel {
	
	var surrenderListener: (Int, CommonProvinceData) => Unit = null
	
	val kingdomLabel = new JLabel
	
	val provinceLabel = new JLabel("<html><b>Province</b></html>")
	val lordLabel = new JLabel("<html><b>Lord</b></html>")
	val sizeLabel = new JLabel("<html><b>Size</b></html>")
	val actionLabel = new JLabel("<html><b>Action</b></html>")
	
	
	
	/**
	 * Adds a listener for land surrendering actions.
	 */
	
	def addSurrenderListener(aSurrenderListener: (Int, CommonProvinceData) => Unit) {
		surrenderListener = aSurrenderListener
	}
	
	
	
	/**
	 * Displays the given kingdom on the pane.
	 */
	
	def display(kingdom: CommonKingdomData, ownProvince: String) {
		kingdomLabel.setText(kingdom.name)
		
		val provincesRowLayoutArray = new Array[Double](2 + kingdom.provinces.size)
		for (i <- 0 until provincesRowLayoutArray.size) {
			provincesRowLayoutArray(i) = 25
		}
		
		setLayout(new TableLayout(Array(0.1, 0.1, 0.05, 0.75), provincesRowLayoutArray))
		
		add(kingdomLabel, "0, 0, 3, 0, CENTER, CENTER")
		add(provinceLabel, "0, 1")
		add(lordLabel, "1, 1")
		add(sizeLabel, "2, 1")
		add(actionLabel, "3, 1")
		
		var i = 2
		kingdom.provinces.values.foreach((province) => {
			if (province.name == kingdom.king) {
				add(new JLabel("<html><font color=\"red\">" + province.name + "</font></html>"), "0, " + i)
				add(new JLabel("<html><font color=\"red\">" + province.lord + "</font></html>"), "1, " + i)
				add(new JLabel("<html><font color=\"red\">" + province.size.toString + "</font></html>"),
						"2, " + i)
			}
			else {
				add(new JLabel(province.name), "0, " + i)
				add(new JLabel(province.lord), "1, " + i)
				add(new JLabel(province.size.toString), "2, " + i)
			}
			add(new ProvinceActionPane(kingdom.provinces(ownProvince), province, surrenderListener),
					"3, " + i)
			i = i + 1
		})
	}
}

