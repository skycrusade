/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view.nearbyprovinces



import net.habraun.skycrusade.common.game._

import java.awt.event._
import javax.swing._
import javax.swing.event._

import info.clearthought.layout._



/**
 * Displays the actions available for a single province.
 */

private[nearbyprovinces] class ProvinceActionPane(ownProvince: CommonProvinceData,
		targetProvince: CommonProvinceData,	surrenderListener: (Int, CommonProvinceData) => Unit)
		extends JPanel {
	
	if (ownProvince == targetProvince) {
		val ownProvinceLabel = new JLabel("This is your own province.")
		
		setLayout(new TableLayout(Array(0.99), Array(0.99)))
		
		add(ownProvinceLabel, "0, 0, 0, 0, LEFT, CENTER")
	}
	else {
		val actions = new JComboBox(Array[Object]("Surrender land"))
		
		val maxSurrenderAmount = ownProvince.size - 20
		if (maxSurrenderAmount >= 1) {
			val amountSlider = new JSlider(1, maxSurrenderAmount, 1)
			val amountField = new JTextField("1", 4)
			val surrenderButton = new JButton("Surrender!")
			
			setLayout(new TableLayout(Array(0.25, 0.4, 0.15, 0.2), Array(0.99)))
			
			add(actions, "0, 0, 0, 0, LEFT, CENTER")
			add(amountSlider, "1, 0, 1, 0, LEFT, CENTER")
			add(amountField, "2, 0, 2, 0, LEFT, CENTER")
			add(surrenderButton, "3, 0, 3, 0, LEFT, CENTER")
			
			amountSlider.addChangeListener(new ChangeListener {
				def stateChanged(event: ChangeEvent) {
					amountField.setText(amountSlider.getValue.toString)
				}
			})
			
			amountField.addActionListener(new ActionListener {
				override def actionPerformed(event: ActionEvent) {
					try {
						amountSlider.setValue(amountField.getText.toInt)
					}
					catch {
						case exception: NumberFormatException => // nothing to do
					}
				}
			})
			
			surrenderButton.addActionListener(new ActionListener {
				def actionPerformed(event: ActionEvent) {
					surrenderListener(amountField.getText.toInt, targetProvince)
				}
			})
		}
		else {
			val noSurrender = new JLabel("Can't surrender any more land.")
			
			setLayout(new TableLayout(Array(0.25, 0.75), Array(0.99)))
			
			add(actions, "0, 0, 0, 0, LEFT, CENTER")
			add(noSurrender, "1, 0, 1, 0, LEFT, CENTER")
		}
	}
}

