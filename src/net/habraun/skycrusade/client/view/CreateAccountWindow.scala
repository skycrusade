/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import java.awt.event._
import javax.swing._

import info.clearthought.layout._



/**
 * Window for creating a new account.
 */

class CreateAccountWindow extends MenuLaunchedFrame {

	setTitle("Create Account")
	
	setLayout(new TableLayout(Array(0.2, 0.6, 0.2), Array(0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14)))
	
	val accountHeadline = new JLabel("<html><u>Account</u></html>")
	
	val accountNameLabel = new JLabel("Name: ")
	val accountName = new JTextField(30)
		
	val passwordLabel = new JLabel("Password: ")
	val password = new JTextField(30)
		
	val provinceHeadline = new JLabel("<html><u>Province</u></html>")
	
	val lordNameLabel = new JLabel("Your name: ")
	val lordName = new JTextField(30)
	
	val provinceNameLabel = new JLabel("Your province: ")
	val provinceName = new JTextField(30)
	
	val createButton = new JButton("Create Account")
	
	add(accountHeadline, "1, 0")
	add(accountNameLabel, "0, 1, 0, 1, RIGHT, CENTER")
	add(accountName, "1, 1, 2, 1, LEFT, CENTER")
	add(passwordLabel, "0, 2, 0, 2, RIGHT, CENTER")
	add(password, "1, 2, 2, 2, LEFT, CENTER")
	add(provinceHeadline, "1, 3")
	add(lordNameLabel, "0, 4, 0, 4, RIGHT, CENTER")
	add(lordName, "1, 4, 2, 4, LEFT, CENTER")
	add(provinceNameLabel, "0, 5, 0, 5, RIGHT, CENTER")
	add(provinceName, "1, 5, 2, 5, LEFT, CENTER")
	add(createButton, "1, 6, 1, 6, CENTER, CENTER")
	
	pack
	
	
	
	/**
	 * Adds a listener that is called when the user attempts to create an account.
	 */
	
	def addAccountCreationListener(listener: (String, String, String, String) => Unit) {
		val actionListener = new ActionListener {
			override def actionPerformed(event: ActionEvent) {
				listener(accountName.getText, password.getText, provinceName.getText, lordName.getText)
				accountName.setText("")
				password.setText("")
				provinceName.setText("")
				lordName.setText("")
			}
		}
		createButton.addActionListener(actionListener)
		accountName.addActionListener(actionListener)
		password.addActionListener(actionListener)
		lordName.addActionListener(actionListener)
		provinceName.addActionListener(actionListener)
	}
}

