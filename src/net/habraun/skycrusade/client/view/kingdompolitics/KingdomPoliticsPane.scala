/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view.kingdompolitics



import net.habraun.skycrusade.common.game._

import javax.swing._

import info.clearthought.layout._



/**
 * Panel for internal kingdom politics.
 */

class KingdomPoliticsPane extends JPanel {

	val kingdomProvinces = new PoliticsProvincesPane
	val kingdomProvincesScrollPane = new JScrollPane(kingdomProvinces)
	
	setLayout(new TableLayout(Array(0.01, 0.98, 0.01), Array(0.99)))
	
	add(kingdomProvincesScrollPane, "1, 0")
	
	
	
	def display(kingdom: CommonKingdomData, ownProvince: String) {
		kingdomProvinces.display(kingdom, ownProvince)
	}
	
	
	
	def addVoteListener(listener: (String) => Unit) {
		kingdomProvinces.addVoteListener(listener)
	}
}

