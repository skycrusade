/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view.kingdompolitics



import net.habraun.skycrusade.common.game._

import java.awt.event._
import javax.swing._

import info.clearthought.layout._



/**
 * List of provinces for the kingdom politics tab.
 */

private[kingdompolitics] class PoliticsProvincesPane extends JPanel {
	
	var listener: (String) => Unit = null
	
	val provinceLabel = new JLabel("Province")
	val lordLabel = new JLabel("Lord")
	val supportsLabel = new JLabel("Supports")
	val votesLabel = new JLabel("Votes")
	
	
	
	def display(kingdom: CommonKingdomData, ownProvince: String) {
		val provincesRowLayoutArray = new Array[Double](1 + kingdom.provinces.size)
		for (i <- 0 until provincesRowLayoutArray.size) {
			provincesRowLayoutArray(i) = 25
		}
		
		setLayout(new TableLayout(Array(0.2, 0.2, 0.4, 0.2), provincesRowLayoutArray))
		
		add(provinceLabel, "0, 0")
		add(lordLabel, "1, 0")
		add(supportsLabel, "2, 0")
		add(votesLabel, "3, 0")
		
		val voteOptionsHead = Array[Object]("No vote", "---")
		val voteOptions = voteOptionsHead ++ kingdom.provinces.keySet.toArray.asInstanceOf[Array[Object]]
		val voteBox = new JComboBox(voteOptions)
		voteBox.setSelectedItem(kingdom.provinces(ownProvince).vote)
		voteBox.addActionListener(new ActionListener {
			def actionPerformed(event: ActionEvent) {
				if (voteBox.getSelectedIndex == 0) {
					listener("")
				}
				else if (voteBox.getSelectedIndex == 1) {
					// do nothing, no valid vote
				}
				else {
					val voteFor = voteBox.getSelectedItem.asInstanceOf[String]
					listener(voteFor)
				}
			}
		})
		
		var i = 1
		kingdom.provinces.values.foreach((province) => {
			// Count how many provinces vote for this province.
			var votes = 0
			kingdom.provinces.values.foreach((votingProvince) => {
				if (votingProvince.vote == province.name) {
					votes += 1
				}
			})
			
			add(new JLabel(province.name), "0, " + i)
			add(new JLabel(province.lord), "1, " + i)
			if (province.name == ownProvince) {
				add(voteBox, "2, " + i)
			}
			else {
				if (province.vote == "") {
					add(new JLabel("<html><font color=\"red\">No vote</font></html>"), "2, " + i)
				}
				else {
					add(new JLabel(province.vote), "2, " + i)
				}
			}
			add(new JLabel(votes.toString), "3, " + i)
			
			i = i + 1
		})
	}
	
	
	
	def addVoteListener(aListener: (String) => Unit) {
		listener = aListener
	}
}

