/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import javax.swing._



/**
 * A frame that is launched from the menu of another frame.
 */

class MenuLaunchedFrame extends JFrame {
	
	setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
	
	private var activatingMenuItem: JMenuItem = null
	
	
	
	/**
	 * Adds a menu item that will be activated when the window is made visible and deactivated when it is
	 * hidden again.
	 */
	
	def addActivatingMenuItem(item: JMenuItem) {
		activatingMenuItem = item
	}
	
	
	
	/**
	 * Overrides setVisible. Enables or disables the activating menu item.
	 */
	
	override def setVisible(visible: Boolean) {
		if (activatingMenuItem != null) {
			activatingMenuItem.setEnabled(!visible)
		}
		
		super.setVisible(visible)
	}
}

