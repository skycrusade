/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import kingdompolitics._
import nearbyprovinces._

import java.awt._
import java.awt.event._
import javax.swing._
import javax.swing.event._



/**
 * The main window.
 */

class MainWindow extends JFrame {
	
	// External windows
	private val aboutWindow = new AboutWindow
	private val loginWindow = new LoginWindow
	private val createAccountWindow = new CreateAccountWindow
	
	// Menu items for the external windows
	private val aboutItem = new JMenuItem("About...")
	private val loginItem = new JMenuItem("Login...")
	private val createAccountItem = new JMenuItem("Create Account...")
	
	private val tabbedPane = new JTabbedPane
	
	val overviewPane = new OverviewPane
	val nearbyProvincesPane = new NearbyProvincesPane
	val kingdomPoliticsPane = new KingdomPoliticsPane
	val workerManagementPane = new WorkerManagementPane
	val globalChatPane = new ChatPane("Global Chat")
	
	init
	
	
	
	/**
	 * Initializes the main window.
	 */
	
	private def init {
		setTitle("Sky Crusade")
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
		setSize(1000, 600)
		
		// Create menu bar
		val menuBar = new JMenuBar
		setJMenuBar(menuBar)
		
		val gameMenu = new JMenu("Game")
		gameMenu.setMnemonic(KeyEvent.VK_G)
		menuBar.add(gameMenu)
		
		gameMenu.add(aboutItem)
		aboutItem.setMnemonic(KeyEvent.VK_A)
		aboutWindow.addActivatingMenuItem(aboutItem)
		aboutItem.addActionListener(new ActionListener {
			def actionPerformed(event: ActionEvent) {
				aboutWindow.setVisible(true)
			}
		})
		
		gameMenu.addSeparator

		gameMenu.add(loginItem)
		loginItem.setMnemonic(KeyEvent.VK_L)
		loginWindow.addActivatingMenuItem(loginItem)
		loginItem.addActionListener(new ActionListener {
			def actionPerformed(event: ActionEvent) {
				loginWindow.setVisible(true)
			}
		})
		
		gameMenu.add(createAccountItem)
		createAccountItem.setMnemonic(KeyEvent.VK_C)
		createAccountWindow.addActivatingMenuItem(createAccountItem)
		createAccountItem.addActionListener(new ActionListener {
			def actionPerformed(event: ActionEvent) {
				createAccountWindow.setVisible(true)
			}
		})
		
		gameMenu.addSeparator
		
		val quitItem = new JMenuItem("Quit")
		quitItem.setMnemonic(KeyEvent.VK_Q)
		gameMenu.add(quitItem)
		quitItem.addActionListener(new ActionListener {
			def actionPerformed(event: ActionEvent) {
				System.exit(0)
			}
		})
		
		tabbedPane.addTab("Overview", overviewPane)
		tabbedPane.addTab("Nearby Provinces", nearbyProvincesPane)
		tabbedPane.addTab("Kingdom Politics", kingdomPoliticsPane)
		tabbedPane.addTab("Worker Management", workerManagementPane)
		tabbedPane.addTab("Global Chat", globalChatPane)
		add(tabbedPane)
		tabbedPane.addChangeListener(new ChangeListener {
			def stateChanged(event: ChangeEvent) {
				tabbedPane.getSelectedComponent.requestFocusInWindow
			}
		})
		
		tabbedPane.addKeyListener(new KeyAdapter {
			override def keyPressed(event: KeyEvent) {
				Console.println("!") 
				
			}
		})
			
		
		KeyboardFocusManager.getCurrentKeyboardFocusManager.addKeyEventDispatcher(new KeyEventDispatcher {
			def dispatchKeyEvent(event: KeyEvent): Boolean = {
				if (event.getModifiers == InputEvent.CTRL_MASK && event.getKeyCode == KeyEvent.VK_Q) {
					System.exit(0)
					true
				}
				else if (event.getModifiers == InputEvent.CTRL_MASK
						&& event.getKeyCode == KeyEvent.VK_RIGHT && event.getID == KeyEvent.KEY_PRESSED) {
					if (tabbedPane.isEnabled) {
						if (tabbedPane.getSelectedIndex < tabbedPane.getTabCount - 1) {
							tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex + 1)
						}
						else {
							tabbedPane.setSelectedIndex(0)
						}
					}
					true
				}
				else if (event.getModifiers == InputEvent.CTRL_MASK
						&& event.getKeyCode == KeyEvent.VK_LEFT && event.getID == KeyEvent.KEY_PRESSED) {
					if (tabbedPane.isEnabled) {
						if (tabbedPane.getSelectedIndex > 0) {
							tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex - 1)
						}
						else {
							tabbedPane.setSelectedIndex(tabbedPane.getTabCount - 1)
						}
					}
					true
				}
				else {
					false
				}
			}
		})
		
		tabbedPane.setEnabled(false)
		setVisible(true)
	}
	
	
	
	/**
	 * Adds the listeners.
	 */
	
	def addListeners(accountCreationListener: (String, String, String, String) => Unit,
			loginListener: (String, String) => Unit) {
		createAccountWindow.addAccountCreationListener(
				(accountName: String, password: String, provinceName: String, lordName: String) => {
					createAccountWindow.setVisible(false)
					accountCreationListener(accountName, password, provinceName, lordName)
				})
		
		loginWindow.addLoginListener(
				(name: String, password: String) => {
					loginWindow.setVisible(false)
					loginListener(name, password)
				})
	}
	
	
	
	/**
	 * Enables the window contents.
	 */
	
	def enableContents {
		loginItem.setEnabled(false)
		createAccountItem.setEnabled(false)
		tabbedPane.setEnabled(true)
	}
}

