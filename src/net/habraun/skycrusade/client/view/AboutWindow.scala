/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import javax.swing._

import info.clearthought.layout._



/**
 * Shows information about the game.
 */

class AboutWindow extends MenuLaunchedFrame {
	
	setLayout(new TableLayout(Array(0.99), Array(0.4, 0.2, 0.2, 0.2)))
	
	val titleLabel = new JLabel("<html><h1>Sky Crusade</h1></html>")
	val copyrightLabel = new JLabel("Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>")
	val sourceLabel = new JLabel("Source code available from: http://repo.or.cz/w/skycrusade.git")
	
	add(titleLabel, "0, 0, 0, 0, CENTER, CENTER")
	add(copyrightLabel, "0, 1, 0, 1, CENTER, CENTER")
	add(sourceLabel, "0, 3, 0, 3, CENTER, CENTER")
	
	pack
}

