/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import net.habraun.skycrusade.common.game._

import java.awt.event._
import javax.swing._
import javax.swing.event._

import info.clearthought.layout._



/**
 * Wind temple control GUI.
 */

class WorkerManagementPane extends JPanel {
	
	var extensionAssignmentListener: (Int) => Unit = null



	val populationLabel = new JLabel("Population:")
	val population = new JLabel
	val freeWorkersLabel = new JLabel("Free workers:")
	val freeWorkers = new JLabel
	val extensionWorkersLabel = new JLabel("Working on province extension:")
	val extensionWorkersField = new JTextField(6)
	val extensionWorkersSlider = new JSlider(0, 1, 0)
	val extensionWorkersButton = new JButton("Assign workers")

	setLayout(new TableLayout(Array(0.25, 0.1, 0.15, 0.2, 0.05, 0.25),
			Array(0.05, 0.05, 0.05, 0.05, 0.8)))

	add(populationLabel, "0, 0, 0, 0, RIGHT, CENTER")
	add(population, "2, 0, 2, 0, LEFT, CENTER")
	add(freeWorkersLabel, "0, 1, 0, 1, RIGHT, CENTER")
	add(freeWorkers, "2, 1, 2, 1, LEFT, CENTER")
	add(extensionWorkersLabel, "0, 3, 0, 3, RIGHT, CENTER")
	add(extensionWorkersField, "2, 3, 2, 3, LEFT, CENTER")
	add(extensionWorkersSlider, "3, 3, 3, 3, LEFT, CENTER")
	add(extensionWorkersButton, "5, 3, 5, 3, LEFT, CENTER")

	extensionWorkersSlider.addChangeListener(new ChangeListener {
		def stateChanged(event: ChangeEvent) {
			extensionWorkersField.setText(extensionWorkersSlider.getValue.toString)
		}
	})

	extensionWorkersField.addActionListener(new ActionListener {
		override def actionPerformed(event: ActionEvent) {
			try {
				extensionWorkersSlider.setValue(extensionWorkersField.getText.toInt)
			}
			catch {
				case exception: NumberFormatException => // nothing to do
			}
		}
	})

	extensionWorkersButton.addActionListener(new ActionListener {
		def actionPerformed(event: ActionEvent) {
			try {
				extensionAssignmentListener(extensionWorkersField.getText.toInt)
			}
			catch {
				case exception: NumberFormatException => // nothing to do
			}
		}
	})
	
	
	
	def addExtensionAssignmentListener(anExtensionAssignmentListener: (Int) => Unit) {
		extensionAssignmentListener = anExtensionAssignmentListener
	}
	
	
	
	def display(ownProvince: OwnProvinceData) {
		population.setText(ownProvince.population.toString)
		freeWorkers.setText((ownProvince.population - ownProvince.extensionWorkers).toString)
		extensionWorkersField.setText(ownProvince.extensionWorkers.toString)
		extensionWorkersSlider.setMaximum(ownProvince.population)
	}
}

