/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import net.habraun.skycrusade.common.game._

import javax.swing._

import info.clearthought.layout._



/**
 * Shows basic information about a province.
 */

class OverviewPane extends JPanel {
	
	// Province information.
	val lordProvinceLabel = new JLabel
	val yourProvinceLabel = new JLabel
	val landLabel = new JLabel
	val populationLabel = new JLabel
	
	// Latest kingdom events.
	val eventList = new JList
	
	setLayout(new TableLayout(Array(0.49, 0.02, 0.49), Array(0.05, 0.05, 0.05, 0.1, 0.75)))
	
	add(lordProvinceLabel, "0, 0, 2, 0, CENTER, CENTER")
	add(yourProvinceLabel, "0, 1, 0, 1, RIGHT, CENTER")
	add(landLabel, "2, 1, 2, 1, LEFT, CENTER")
	add(populationLabel, "2, 2, 2, 2, LEFT, CENTER")
	add(eventList, "0, 4, 2, 4, CENTER, TOP")
	
	
	
	/**
	 * Display the province data.
	 */
	
	def displayProvinceData(province: CommonProvinceData) {
		lordProvinceLabel.setText("<html>Lord <u>" + province.lord + "</u> of <u>" + province.name
				+ "</u></html>")
		landLabel.setText("<html>Your province: <u>" + province.size + "</u> square kilometers</html>")
	}
	
	
	
	/**
	 * Display the province data.
	 */
	
	def displayProvinceData(province: OwnProvinceData) {
		lordProvinceLabel.setText("<html>Lord <u>" + province.lord + "</u> of <u>" + province.name
				+ "</u></html>")
		yourProvinceLabel.setText("Your province:")
		landLabel.setText("<html><u>" + province.size + "</u> square kilometers</html>")
		populationLabel.setText("<html><u>" + province.population + "</u> citizens")
	}
	
	
	
	/**
	 * Display the latest events.
	 */
	
	def displayEvents(events: Array[String]) {
		eventList.setListData(events.asInstanceOf[Array[Object]])
	}
}

