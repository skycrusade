/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import java.awt.event._
import javax.swing._

import info.clearthought.layout._



/**
 * Window for logging into an account.
 */

class LoginWindow extends MenuLaunchedFrame {

	setTitle("Login")
	
	setLayout(new TableLayout(Array(0.2, 0.6, 0.2), Array(0.33, 0.33, 0.33)))
	
	val nameLabel = new JLabel("Name: ")
	val name = new JTextField(30)
	
	val passwordLabel = new JLabel("Password: ")
	val password = new JPasswordField(30)
	
	val loginButton = new JButton("Login")
	
	add(nameLabel, "0, 0, 0, 0, RIGHT, CENTER")
	add(name, "1, 0, 2, 0, LEFT, CENTER")
	add(passwordLabel, "0, 1, 0, 1, RIGHT, CENTER")
	add(password, "1, 1, 2, 1, LEFT, CENTER")
	add(loginButton, "1, 2, 1, 2, CENTER, CENTER")
	
	pack
	
	
	
	/**
	 * Adds a listener that is called when the user attempts to log in.
	 */
	
	def addLoginListener(listener: (String, String) => Unit) {
		val actionListener = new ActionListener {
			override def actionPerformed(event: ActionEvent) {
				listener(name.getText, password.getText)
			}
		}
		loginButton.addActionListener(actionListener)
		name.addActionListener(actionListener)
		password.addActionListener(actionListener)
	}
}

