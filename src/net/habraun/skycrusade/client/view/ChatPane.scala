/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client.view



import net.habraun.skycrusade.common.game._

import java.awt._
import java.awt.event._
import javax.swing._

import info.clearthought.layout._



/**
 * A chat pane that can be used for text chats.
 */

class ChatPane(title: String) extends JPanel {
	
	val titleLabel = new JLabel(title)
	
	val textArea = new JTextArea
	textArea.setEditable(false)
	val scrollPane = new JScrollPane(textArea)
	
	val inputField = new JTextField(50)
	val sendButton = new JButton("Send message")
	
	setLayout(new TableLayout(Array(0.01, 0.98, 0.01), Array(0.05, 0.85, 0.05, 0.05)))
	
	add(titleLabel, "1, 0, 1, 0, CENTER, CENTER")
	add(scrollPane, "1, 1")
	add(inputField, "1, 2, 1, 2, CENTER, CENTER")
	add(sendButton, "1, 3, 1, 3, CENTER, CENTER")
	
	addFocusListener(new FocusListener {
		def focusGained(event: FocusEvent) {
			inputField.requestFocusInWindow
		}
		
		def focusLost(event: FocusEvent) {}
	})
	
	
	
	/**
	 * Displays the message in the text area.
	 */
	
	def showMessage(sender: CommonProvinceData, message: String) {
		textArea.append(sender.lord + " of " + sender.name + ": " + message + "\n")
		val scrollBar = scrollPane.getVerticalScrollBar
		scrollBar.setValue(scrollBar.getMaximum)
	}
	
	
	
	/**
	 * Adds a listener that is called when a message is sent.
	 */
	
	def addMessageListener(listener: (String) => Unit) {
		val processMessage = () => {
			val text = inputField.getText
			if (text != "") {
				listener(text)
				inputField.setText("")
			}
		}
			
		sendButton.addActionListener(new ActionListener {
			override def actionPerformed(event: ActionEvent) {
				processMessage()
			}
		})
		
		inputField.addActionListener(new ActionListener {
			override def actionPerformed(event: ActionEvent) {
				processMessage()
			}
		})
	}
}

