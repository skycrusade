/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client



import view._
import net.habraun.skycrusade.common._
import net.habraun.skycrusade.common.game._

import java.net._

import net.habraun.libcrusade.network.client._



/**
 * The main object and entry point for the Sky Crusade client application.
 */

object SkyCrusadeClient extends Application {
	
	val mainWindow = new MainWindow
	val handler = new Handler(mainWindow)
	val server = new ServerConnection(new InetSocketAddress("localhost", 34482), handler)
	
	mainWindow.addListeners(
			(accountName: String, password: String, provinceName: String, lordName: String) => {
				server  ! Send(CreateAccountMessage(accountName, password, provinceName, lordName))
			},
			(name: String, password: String) => {
				server ! Send(LoginMessage(name, password))
			})
	
	mainWindow.globalChatPane.addMessageListener((message: String) =>
		server ! Send(TextMessage(message)))
	
	mainWindow.nearbyProvincesPane.addSurrenderListener(
			(amount: Int, targetProvince: CommonProvinceData) =>
				server ! Send(SurrenderLand(amount, targetProvince.name)))
	
	mainWindow.kingdomPoliticsPane.addVoteListener(
			(vote: String) =>
				server ! Send(ChangeVoteMessage(vote)))
	
	mainWindow.workerManagementPane.addExtensionAssignmentListener(
			(amount: Int) =>
				server ! Send(AssignExtensionWorkersMessage(amount)))
}

