/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.client



import view._
import net.habraun.skycrusade.common._
import net.habraun.skycrusade.common.game._

import javax.swing._

import net.habraun.libcrusade.network.client._



/**
 * Handles incoming server messages.
 */

class Handler(mainWindow: MainWindow) extends ServerHandler {
	
	var ownProvince: OwnProvinceData = null
	
	
	
	override def messageReceived(message: Any) {
		SwingUtilities.invokeLater(new Runnable {
			def run {
				message match {
					case BroadcastedTextMessage(sender, textMessage) =>
						mainWindow.globalChatPane.showMessage(sender, textMessage)
						
					case LoggedInMessage(successful) =>
						if (successful) {
							mainWindow.enableContents
						}
					
					case province: OwnProvinceData =>
						ownProvince = province
						mainWindow.overviewPane.displayProvinceData(province)
						mainWindow.workerManagementPane.display(province)
					
					case kingdom: CommonKingdomData =>
						mainWindow.overviewPane.displayEvents(kingdom.events.toArray)
						mainWindow.nearbyProvincesPane.display(kingdom, ownProvince.name)
						mainWindow.kingdomPoliticsPane.display(kingdom, ownProvince.name)
						
					case _ =>
						Console.println("Unknown message: " + message)
				}
			}
		})
	}
	
	
	
	override def connectionClosed {
		System.exit(0)
	}
}

