/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



import scala.actors._
import scala.collection.immutable._

import org.junit._
import org.junit.Assert._



class StorageRetrieverTest {
	
	val storedKey = "storedKey"
	val storedValue = 0
	
	
	
	var retriever: StorageRetriever = null
	
	
	
	@Before
	def setup {
		// Setup for testing retrieveFromStorage.
		val storage = new Actor {
			def act {
				react {
					case Retrieve("storedKey") =>
						reply(storedValue)
					case Retrieve(_) =>
						reply(null)
				}
			}
		}
		storage.start
		
		retriever = new StorageRetriever(storage)
	}
	
	
	
	@Test
	def retrieveNonExistingObjectExpectNone {
		assertEquals(None, retriever("noKey"))
	}
	
	
	
	@Test
	def retrieveStoredValueExpectSuccess {
		assertEquals(storedValue, retriever(storedKey).getOrElse { throw new AssertionError })
	}
}

