/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.libcrusade.persistence.filter



import org.junit._
import org.junit.Assert._



class TransactionEvaluatorTest {
	
	val trueComparator = (p: Option[Any], c: Any) => true
	
	val someRetriever = (k: String) => Some(1)
	
	val singleTransaction = ("key", 0)::Nil
	val multiTransaction = ("key1", 1)::("key2", 2)::Nil
	
	
	
	@Test
	def evaluateSingleExistingObjectComparatorReturnsTrue {
		val evaluate = new TransactionEvaluator(trueComparator, someRetriever)
		
		assertTrue(evaluate(singleTransaction))
	}
	
	
	
	@Test
	def evaluateSingleExistingObjectComparatorReturnsFalse {
		val comparator = (p: Option[Any], c: Any) => false
		
		val evaluate = new TransactionEvaluator(comparator, someRetriever)
		
		assertFalse(evaluate(singleTransaction))
	}
	
	
	
	@Test
	def evaluateMultipleExistingObjectsComparatorReturnsTrue {
		val evaluate = new TransactionEvaluator(trueComparator, someRetriever)
		
		assertTrue(evaluate(multiTransaction))
	}
	
	
	
	@Test
	def evaluateMultipleExistingObjectsComparatorReturnsFalseOnce {
		val comparator = (p: Option[Any], c: Any) => if (c == 1) false else true
		
		val evaluate = new TransactionEvaluator(comparator, someRetriever)
		
		assertFalse(evaluate(multiTransaction))
	}
	
	
	
	@Test
	def evaluateMultipleExistingObjectsComparatorComparesRealistically {
		val comparator = (p: Option[Any], c: Any) => p.asInstanceOf[Some[Int]].getOrElse
				{ throw new AssertionError } + 1 == c.asInstanceOf[Int]
		val retriever = (k: String) => k match { case "key1" => Some(0) case "key2" => Some(1) }
		
		val evaluate = new TransactionEvaluator(comparator, retriever)
		
		assertTrue(evaluate(multiTransaction))
	}
	
	
	
	@Test
	def evaluateNonExistingObjectComparatorExpectsNoneAsPrevious {
		val comparator = (p: Option[Any], c: Any) => p == None
		val retriever = (k: String) => None
		
		val evaluate = new TransactionEvaluator(comparator, retriever)
		
		assertTrue(evaluate(multiTransaction))
	}
}

