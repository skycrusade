/*
	Copyright (c) 2008, 2009 Hanno Braun <hanno@habraun.net>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/



package net.habraun.skycrusade.server.game



import org.junit._
import org.junit.Assert._



class ProvinceTest extends ProvinceConstants {

	var province: Province = null



	@Before
	def setup {
		province = new Province("province", "lord")
	}
	
	
	
	@Test
	def updateCheckRegularPopulationGrowth {
		val newProvince = province.update
		val expectedNewPopulation = (province.population * (1 + POPULATION_REGULAR_GROWTH)).asInstanceOf[Int]
		assertEquals(expectedNewPopulation, newProvince.population)
	}



	@Test
	def updateCheckMinimumPopulationGrowth {
		val newProvince = Province(0, province.name, province.lord, province.size,
				POPULATION_MIN_GROWTH_THRESHOLD, province.extensionWorkers, province.vote)
		val updatedProvince = newProvince.update
		assertEquals(newProvince.population + POPULATION_MIN_GROWTH, updatedProvince.population)
	}



	@Test
	def updateCheckMaximumPopulation {
		val newProvince = Province(0, province.name, province.lord,	province.size,
				province.size * POPULATION_MAX_PER_SQKM, province.extensionWorkers, province.vote)
		val updatedProvince = newProvince.update
		assertEquals(newProvince.population, updatedProvince.population)
	}



	@Test
	def assignTooManyExtensionWorkers {
		val newProvince = province.assignExtensionWorkers(province.population + 1000)
		assertEquals(None, newProvince)
	}



	@Test
	def assignExtensionWorkersCheckIfAssigned {
		val newProvince = province.assignExtensionWorkers(province.population).getOrElse {
			throw new AssertionError
		}
		assertEquals(newProvince.population, newProvince.extensionWorkers)
	}



	@Test
	def assignExtensionWorkersTwice {
		val newProvince = province.assignExtensionWorkers(province.population).getOrElse {
			throw new AssertionError
		}
		val newNewProvince = newProvince.assignExtensionWorkers(province.population - 1).getOrElse {
			throw new AssertionError
		}
		assertEquals(province.population - 1, newNewProvince.extensionWorkers)
	}



	@Test
	def assignExtensionWorkersCheckGrowthOnUpdate {
		val expectedNewSize = province.size + (EXTENSION_PER_WORKER * province.population).asInstanceOf[Int]

		val newProvince = province.assignExtensionWorkers(province.population).getOrElse {
			throw new AssertionError
		}
		val updatedProvince = newProvince.update

		assertEquals(expectedNewSize, updatedProvince.size)
	}



	@Test
	def assignExtensionWorkersCheckPopulationGrowth {
		val newProvince = province.assignExtensionWorkers(province.population).getOrElse {
			throw new AssertionError
		}
		val updatedProvince = newProvince.update

		assertEquals(newProvince.population, updatedProvince.population)
	}
}
